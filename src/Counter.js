import React, { useState } from "react";
import { Button } from "react-bootstrap";

const Counter = () => {
  const [counter, setCounter] = useState(0);

  const incrementCounter = () => {
    setCounter((prevCounter) => prevCounter + 1);
  };

  const decrementCounter = () => {
    setCounter((prevCounter) => prevCounter - 1);
  };
  return (
    <>
      <Button data-testid="increment" onClick={incrementCounter}>
        {" "}
        +{" "}
      </Button>
      <p data-testid="counter">{counter}</p>
      <Button data-testid="decrement" onClick={decrementCounter}>
        {" "}
        -{" "}
      </Button>
    </>
  );
};

export default Counter;
