const Dog = require("./Dog");

describe("Dog", () => {
  it("Should Arnold", () => {
    const dog = new Dog("Arnold");

    expect(dog).toHaveProperty("name", "Arnold");
  });

  it("Woff", () => {
    const dog = new Dog("Arnold");
    expect(dog.brack()).toEqual("hahahha");
  });
});
